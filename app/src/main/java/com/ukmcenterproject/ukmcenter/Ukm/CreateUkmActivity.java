package com.ukmcenterproject.ukmcenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class CreateUkmActivity extends AppCompatActivity {

    ImageView ivGambarProfile;
    private static final int PICK_IMAGE = 100;
    private final int REQUEST_IMAGE_CAPTURE = 1;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_ukm);

        getSupportActionBar().setTitle("Create UKM");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ivGambarProfile = (ImageView)findViewById(R.id.ivGambarProfile);
    }

    public void openGallery(View view){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , PICK_IMAGE);
    }

    public void capturePhoto(View view){
        Intent takePicture = new Intent( MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            ivGambarProfile.setImageURI(imageUri);
        }else if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                ivGambarProfile.setImageBitmap(imageBitmap);

        }else {
            Toast.makeText(this, "Nothing Photo", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}

































































