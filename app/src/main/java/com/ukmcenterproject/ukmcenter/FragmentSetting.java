package com.ukmcenterproject.ukmcenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by tangguhjp on 07/12/17.=
 */

public class FragmentSetting extends Fragment {
    private TextView bt_edit_user;
    private TextView bt_edit_ukm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        bt_edit_user = view.findViewById(R.id.tv_edit_account);
        bt_edit_ukm = view.findViewById(R.id.tv_edit_ukm);

        return view;
    }
}