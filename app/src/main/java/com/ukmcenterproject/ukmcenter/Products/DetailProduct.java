package com.ukmcenterproject.ukmcenter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ukmcenterproject.ukmcenter.ApiHelper.UtilsApi;

public class DetailProduct extends AppCompatActivity {
    private static final String BASE_URL = UtilsApi.BASE_URL_IMAGE;
    private static final String TAG = "DetailProduct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Details Product");

        Intent intent = getIntent();
        String img = intent.getStringExtra("img_url");
        String productName = intent.getStringExtra("product_name");
        // Capture the layout's TextView and set the string as its text

        TextView textView = (TextView) findViewById(R.id.productname);
        TextView ukmName = findViewById(R.id.ukmname);
        ImageView imageView = findViewById(R.id.imgproduct);
        TextView description = findViewById(R.id.description);

        textView.setText(productName);
        ukmName.setText(intent.getStringExtra("ukmName"));
        Picasso.with(getApplicationContext())
                .load(BASE_URL+img)
                .into(imageView);
        description.setText(intent.getStringExtra("description"));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}

