package com.ukmcenterproject.ukmcenter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tangguhjp on 05/12/17.
 */

public class AddProductActivity extends AppCompatActivity {
    private static final String TAG = "AddProductActivity";
    private ImageView iv_fproduct;
    private static final int PICK_IMAGE = 100;
    private final int REQUEST_IMAGE_CAPTURE = 1;
    Uri imageUri;
    private Spinner categories_spiner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_add_product);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        iv_fproduct = findViewById(R.id.iv_fproduct);
        categories_spiner = findViewById(R.id.sp_categories);
        addItemsOnSpinner();
        showMessageCategory();
    }

    public void openGallery(View v){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , PICK_IMAGE);
    }

    public void takePhoto(View v){
        Intent takePicture = new Intent( MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            iv_fproduct.setImageURI(imageUri);
        }else if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            iv_fproduct.setImageBitmap(imageBitmap);

        }else {
            Toast.makeText(this, "Nothing Photo", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /**
     * Spinner Dropdown to choose Category Product
     */
    public void addItemsOnSpinner(){
        List<String> categories = new ArrayList<>();
        categories.add("Pertanian");
        categories.add("Perikanan");
        categories.add("Kerajinan");
        categories.add("Peternakan");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Set Array Adapter on Spinner
        categories_spiner.setAdapter(dataAdapter);

    }

    public void showMessageCategory(){
        categories_spiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(AddProductActivity.this, "Category : "+String.valueOf(categories_spiner.getSelectedItem()),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
