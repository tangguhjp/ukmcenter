package com.ukmcenterproject.ukmcenter;

/**
 * Created by tangguhjp on 29/11/17.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.ukmcenterproject.ukmcenter.ApiHelper.UtilsApi;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> implements Filterable {
    private static final String TAG = "ProductAdapter";
    private static final String BASE_URL = UtilsApi.BASE_URL_IMAGE;

    List<com.ukmcenterproject.ukmcenter.Product> list;
    private Context context;

    private static ArrayList<com.ukmcenterproject.ukmcenter.Product> dataProduct;

    public ProductAdapter(List<com.ukmcenterproject.ukmcenter.Product> list, Context context){
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.myproduct_card, parent, false);
        String className=v.getContext().getClass().getSimpleName();
        if (className.equalsIgnoreCase("MyProductActivity")){
            v.setOnClickListener(com.ukmcenterproject.ukmcenter.MyProductActivity.productOnClickListener);
        }else{
            v.setOnClickListener(com.ukmcenterproject.ukmcenter.ProductListActivity.productOnClickListener);
        }
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        com.ukmcenterproject.ukmcenter.Product product= list.get(position);
        holder.tvProductName.setText(product.getProductName());
        holder.tvProductPrice.setText("RP. "+product.getProductPrice());
        holder.tvUkmName.setText(product.getProductUkm());
        Picasso.with(context)
                .load(BASE_URL+product.getProductImgUrl())
                .into(holder.ivProductImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String string = constraint.toString();
                if (string.isEmpty()){
                    dataProduct= (ArrayList<com.ukmcenterproject.ukmcenter.Product>) list;
                }else {
                    ArrayList<com.ukmcenterproject.ukmcenter.Product> filteredList = new ArrayList<>();
                    for (int i=0; i<list.size();i++){
                        com.ukmcenterproject.ukmcenter.Product product = list.get(i);
                        if (product.getProductName().toLowerCase().contains(string)){
                            Log.i(" FILTERED", product.getProductName());
                            filteredList.add(product);
                        }
                    }
                    dataProduct = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values=dataProduct;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataProduct = (ArrayList<com.ukmcenterproject.ukmcenter.Product>) results.values;
                notifyDataSetChanged();
            }
        };

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivProductImage,delete,update;
        private TextView tvProductName, tvProductPrice, tvUkmName;
        public ViewHolder(View itemView) {
            super(itemView);
            this.ivProductImage = itemView.findViewById(R.id.iv_product_image);
            this.tvProductName = itemView.findViewById(R.id.tv_product_name);
            this.tvProductPrice = itemView.findViewById(R.id.tv_product_price);
            this.tvUkmName = itemView.findViewById(R.id.iv_ukm_name_card);
            this.delete = itemView.findViewById(R.id.iv_delete_product);
            this.update = itemView.findViewById(R.id.iv_update_product);
            if (itemView.getContext().getClass().getSimpleName().equalsIgnoreCase("ProductListActivity")) {
                this.delete.setVisibility(View.INVISIBLE);
                this.update.setVisibility(View.INVISIBLE);
            }
        }
    }

}

