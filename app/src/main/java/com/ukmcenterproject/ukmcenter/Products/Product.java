package com.ukmcenterproject.ukmcenter;

/**
 * Created by tangguhjp on 29/11/17.
 */

public class Product {
//    private int image;
//    private String name;
//    private String price;
//    private int owner;
    int productId;
    int productCategory;
    String productUkm;
    String productName;
    String productDesc;
    String productPrice;
    String productImgUrl;

    public Product() {

    }

    public Product(int productId, int productCategory, String productUkm, String productName, String productDesc, String productPrice, String productImgUrl) {
        this.productId = productId;
        this.productCategory = productCategory;
        this.productUkm = productUkm;
        this.productName = productName;
        this.productDesc = productDesc;
        this.productPrice = productPrice;
        this.productImgUrl = productImgUrl;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setProductCategory(int productCategory) {
        this.productCategory = productCategory;
    }

    public void setProductUkm(String productUkm) {
        this.productUkm = productUkm;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public int getProductId() {
        return productId;
    }

    public int getProductCategory() {
        return productCategory;
    }

    public String getProductUkm() {
        return productUkm;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }


//    public Product(int image, String name, String price) {
//        this.image = image;
//        this.name = name;
//        this.price = price;
//    }
//
//    public int getImage() {
//        return image;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getPrice() {return price; }
//
//    public int getOwner(){return owner;}
}


