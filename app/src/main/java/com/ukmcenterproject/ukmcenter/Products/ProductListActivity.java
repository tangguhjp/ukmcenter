package com.ukmcenterproject.ukmcenter;

/**
 * Created by tangguhjp on 29/11/17.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.ukmcenterproject.ukmcenter.ApiHelper.BaseApiService;
import com.ukmcenterproject.ukmcenter.ApiHelper.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends AppCompatActivity {
    private static final String TAG = "ProductListActivity";

    public static int currentItem;
    static View.OnClickListener productOnClickListener;

    private RecyclerView recyclerView;
    List<com.ukmcenterproject.ukmcenter.Product> listing;

    SearchView searchView;
    com.ukmcenterproject.ukmcenter.ProductAdapter productAdapter;

    BaseApiService mApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Products List");

        productOnClickListener = new ProductOnClickListener(this);
        searchView = findViewById(R.id.search);
        recyclerView = findViewById(R.id.rv_product_list);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        listing = new ArrayList<>();
        mApiService = UtilsApi.getAPIService();

        initComponents();
    }

    private void initComponents() {
        Call<String> call = mApiService.getProduct();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call,@NonNull Response<String> response) {
                if(response.isSuccessful()){
                    String jsonResponse = response.body();
                    try {
                        JSONArray jsonArray = new JSONArray(jsonResponse);
                        if (jsonArray.length()>0){
                            com.ukmcenterproject.ukmcenter.Product product;
                            for (int i = 0; i<jsonArray.length();i++){
                                product = new com.ukmcenterproject.ukmcenter.Product();
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                JSONObject ukm = new JSONObject(jsonObject.getString("product_ukm"));
                                Log.i(TAG, "UKM: "+ukm.getString("ukm_name"));
                                product.setProductName(jsonObject.getString("product_name"));
                                product.setProductPrice(jsonObject.getString("product_price"));
                                product.setProductImgUrl(jsonObject.getString("product_foto"));
                                product.setProductDesc(jsonObject.getString("product_description"));
                                product.setProductUkm(ukm.getString("ukm_name"));
                                listing.add(product);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    productAdapter = new com.ukmcenterproject.ukmcenter.ProductAdapter(listing, getApplicationContext());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(productAdapter);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return  super.onOptionsItemSelected(item);
    }

    private class ProductOnClickListener implements View.OnClickListener {
        private final Context context;

        private ProductOnClickListener(Context c) {
            this.context = c;
        }

        @Override
        public void onClick(View view) {
            currentItem = recyclerView.getChildAdapterPosition(view);
            com.ukmcenterproject.ukmcenter.Product product = listing.get(currentItem);

            Intent intent = new Intent(getApplication(), com.ukmcenterproject.ukmcenter.DetailProduct.class);

            intent.putExtra("product_name", product.getProductName());
            intent.putExtra("img_url", product.getProductImgUrl());
            intent.putExtra("description",product.getProductDesc());
            intent.putExtra("ukmName", product.getProductUkm());

            startActivity(intent);
        }
    }

    private void search(SearchView searchView){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                productAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
