package com.ukmcenterproject.ukmcenter.ApiHelper;

/**
 * Created by tangguhjp on 28/12/17.
 */

public class UtilsApi{
    // 10.0.2.2 ini adalah localhost.
    public static final String BASE_URL_API = "http://192.168.43.169:7000";
    public static final String BASE_URL_IMAGE = "http://192.168.43.169:8888";
    public static String BEARER = "";
    public static String ID_USER="";

    // Mendeklarasikan Interface BaseApiService
    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }

    public static String getBearer(){
        return BEARER;
    }

    public static String getIdUser(){
        return ID_USER;
    }
}
