package com.ukmcenterproject.ukmcenter.ApiHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by tangguhjp on 28/12/17.
 */

public interface BaseApiService {
    // Fungsi ini untuk memanggil API http://10.0.2.2/mahasiswa/login.php
    @FormUrlEncoded
    @POST("/user/login")
    Call<ResponseBody> loginRequest(@Field("LoginForm[username]") String email,
                                                    @Field("LoginForm[password]") String password);

    // Fungsi ini untuk memanggil API http://10.0.2.2/mahasiswa/register.php
    @FormUrlEncoded
    @POST("/user/create")
    Call<ResponseBody> registerRequest(@Field("User[fullname]") String fullname,
                                                @Field("User[username]") String username,
                                                @Field("User[email]") String email,
                                                @Field("User[password]") String password,
                                                @Field("User[status]") Integer status);

    @GET("/product")
    Call<String> getProduct();

}

