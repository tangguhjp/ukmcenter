package com.ukmcenterproject.ukmcenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ukmcenterproject.ukmcenter.ApiHelper.BaseApiService;
import com.ukmcenterproject.ukmcenter.ApiHelper.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tangguhjp on 20/10/17.
 */

public class Register extends Fragment implements View.OnClickListener{
    private static final String TAG = "Register";
    public Register(){}
    EditText etFullname;
    EditText etUsername;
    EditText etEmail;
    EditText etPassword;
    EditText etPassword_confirm;
    Button btnRegister;
    ProgressDialog loading;
    Integer STATUS=10;

    Context mContext;
    BaseApiService mApiService;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register, container, false);
        etFullname = view.findViewById(R.id.et_fullname);
        etUsername = view.findViewById(R.id.et_uname);
        etEmail = view.findViewById(R.id.et_mail);
        etPassword = view.findViewById(R.id.et_passwd);
        etPassword_confirm = view.findViewById(R.id.et_confirm_pass);
        btnRegister = view.findViewById(R.id.bt_sign_up);
        Log.i(TAG, "onCreateView: "+btnRegister);
        
        
        mContext = getContext();
        mApiService = UtilsApi.getAPIService();
        initComponents();
        
//
//        Button button = view.findViewById(R.id.bt_sign_up);
//
//        button.setOnClickListener(this);
        return view;
    }

    private void initComponents() {

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etFullname.getText().toString().isEmpty() ||
                        etEmail.getText().toString().isEmpty()||
                        etUsername.getText().toString().isEmpty()||
                        etPassword.getText().toString().isEmpty()){
                    Toast.makeText(mContext, "Please, Input Data Correctly", Toast.LENGTH_SHORT).show();
                }else {
                    if(etPassword.getText().toString().equals(etPassword_confirm.getText().toString())){
                        loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                        requestRegister();
                    }else{
                        Toast.makeText(mContext, "Incorrect Password Confirm", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

    private void requestRegister(){
        mApiService.registerRequest(etFullname.getText().toString(),
                etUsername.getText().toString(),
                etEmail.getText().toString(),
                etPassword.getText().toString(),
                STATUS)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            Log.i("debug", "onResponse: BERHASIL");
                            loading.dismiss();
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("msg").equals("success")){
                                    Toast.makeText(mContext, "BERHASIL REGISTRASI", Toast.LENGTH_SHORT).show();
                                    ViewPager mPager = getActivity().findViewById(R.id.container);
                                    mPager.setCurrentItem(0);
                                } else {
                                    String error_message = jsonRESULTS.getString("error_msg");
                                    Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.i("debug", "onResponse: GA BERHASIL");
                            loading.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "onFailure: ERROR > " + t.getMessage());
                        Toast.makeText(mContext, "Koneksi Internet Bermasalah", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        
    }


//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.bt_sign_up:
//                ViewPager mPager = getActivity().findViewById(R.id.container);
//                mPager.setCurrentItem(0);
//                break;
//        }
//    }
}
