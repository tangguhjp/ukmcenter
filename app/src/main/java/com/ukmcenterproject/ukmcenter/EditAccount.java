package com.ukmcenterproject.ukmcenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by tangguhjp on 07/12/17.
 */

public class EditAccount extends Fragment {
    private Button bt_save;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_account, container, false);
        bt_save = view.findViewById(R.id.bt_save_edited);

        return view;
    }
}
